from models.sentence_to_sql import SentenceToSql
import spacy
import sys


def print_sql_sentence(sentence_to_sql):
    print(select_format.format(sentence_to_sql.get_select_clause(),
                               sentence_to_sql.get_table_name(),
                               sentence_to_sql.get_where_clause()))

if __name__ == "__main__":
    nlp = spacy.load('en_core_web_lg')

    select_format = "select {0}\nfrom table {1}\nwhere {2}"
    columns = ["Company", "advertise_with_google", "advertise_with_facebook", "CEO", "money_raised"]
    columns_types = ["String", "Money", "Money", "String", "Money"]

    sentences = ["Show me all the companies that advertise with Google with more than $100K",
                 "i want all companies which had an advertisement campaigns with google with more than $100K",
                 "give me a company which advertise at google and spent more then $100k",
                 "Show me all the companies that promotes with Google with more than $100K",
                 "Show me all the companies that advertised with google with more than $100K",
                 "Show me all the companies that advertised with google with less than $100K",
                 "Show me all the companies that advertised with google with exactly $100K",
                 "Show me all the companies that advertised with google with $100K",
                 "I want the all the companies that have been advertising with facebook with more than $30k"
                 ]
    expected_out_put = """
        select company
         from table
         where advertise_with_google > 100000;
    """

    sentence_to_sql = SentenceToSql("SomeTable", columns, columns_types, sentences[3], nlp)
    print_sql_sentence(sentence_to_sql)

    # for sentence in sentences:
    #     sentence_to_sql = SentenceToSql("SomeTable", columns, sentence, nlp)
    #     # sentence_to_sql.print_tokens()
    #     # sentence_to_sql.print_labels()
    #     print_sql_sentence(sentence_to_sql)

