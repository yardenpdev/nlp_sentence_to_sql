# from titlecase import titlecase
from spacy.matcher import PhraseMatcher

class SentenceToSql(object):

    def __init__(self, table, columns, columns_types, sentence, nlp):
        self.nlp = nlp
        self.table = table
        self.columns_names = columns
        self.columns_docs = [nlp(self.simplify_sent(column)) for column in columns]
        self.columns_types = columns_types
        [self.print_tokens(doc) for doc in self.columns_docs]
        [self.print_labels(doc) for doc in self.columns_docs]
        # print(self.nlp.vocab[u'dog'].similarity(nlp.vocab[u'dachshund']))
        # print([w.lower_ for w in self.get_related(self.nlp.vocab[u'advertise'])])
        # print([w.lower_ for w in self.get_related(self.nlp.vocab[u'promote'])])
        print(self.nlp.vocab[u'advertise'].similarity(nlp.vocab[u'promote']))
        print(self.nlp.vocab[u'advertise'].similarity(nlp.vocab[u'dog']))
        print(self.nlp.vocab[u'advertise'].similarity(nlp.vocab[u'shit']))
        print(self.nlp.vocab[u'advertise'].similarity(nlp.vocab[u'love']))
        print(self.nlp.vocab[u'advertise'].similarity(nlp.vocab[u'lion']))
        print(self.nlp.vocab[u'dog'].similarity(nlp.vocab[u'lion']))
        doc1 = self.nlp("promotes with Google")
        doc2 = self.nlp("advertised with facebook")



        # create a matcher
        (self.matcher, self.column_rule_id_to_name) = self.create_column_matcher(self.columns_docs)

        self.doc = nlp(sentence.lower())
        sentence_simp = self.simplify_sent(sentence)
        print("Simplified Sentence:", sentence_simp)
        self.doc_simp = nlp(sentence_simp)

        self.column_matches = self.get_column_matches(self.doc_simp)
        [print("Matched:", self.doc[match[1]:match[2]]) for match in self.column_matches]

        print("doc:",self.doc.text)
        self.print_labels(self.doc_simp)
        print("doc_simple:",self.doc_simp.text)
        self.print_labels(self.doc_simp)

        for column_doc in self.columns_docs:
            print(column_doc)
            print(column_doc.similarity(self.doc_simp))


    def create_column_matcher(self, docs):
        matcher = PhraseMatcher(self.nlp.vocab)
        rule_ids = []
        for count, col_doc in enumerate(docs):
            rule_id = u'COL'+str(count)
            matcher.add(rule_id, self.on_match, col_doc)
            rule_ids.append(rule_id)
        column_rule_id_to_name = dict(zip(rule_ids, self.columns_names))
        return matcher, column_rule_id_to_name

    def on_match(self, matcher, doc, id, matches):
        pass

    def get_column_matches(self, doc):
        # use elastic search proximity mapping
        return self.matcher(doc)

    def get_select_clause(self):
        if len(self.column_matches) > 0: # this should be chagned to understand the sentence better
            string_id = self.get_rule_string_id(self.column_matches[0])
            return self.column_rule_id_to_name.get(string_id)
        return "*"

    def get_rule_string_id(self, match):
        match_id = match[0]
        string_id = self.doc_simp.vocab.strings[match_id]
        return string_id

    # returns where clause
    def get_where_clause(self):
        return "get_where_clause"

    def get_table_name(self):
        return self.table

    def print_labels(self, doc):
        for ent in doc.ents:
            print("ENTITY:", ent.text, ent.start_char, ent.end_char, ent.label_)

    def print_tokens(self, doc):
        for token in doc:
            print("TOKEN:", token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
                  token.shape_, token.is_alpha, token.is_stop)

    def print_similarity(self, sentence_other, doc):
        doc_other = self.nlp(sentence_other)
        similarity = doc.similarity(doc_other)
        print("SIMILARITY:", doc.text, doc_other.text, similarity)

    def lemmatize_sent(self, sentence):
        doc = self.nlp(sentence)
        new_sentence = []
        for token in doc:
            new_sentence.append(token.lemma_)
        return ' '.join(new_sentence)

    def get_related(self, word):
        filtered_words = [w for w in word.vocab if w.is_lower == word.is_lower and w.prob >= -15]
        similarity = sorted(filtered_words, key=lambda w: word.similarity(w), reverse=True)
        return similarity[:10]


    def get_main_subject(self, doc):
        from spacy.symbols import nsubj, VERB

        # Finding a verb with a subject from below — good
        verbs = set()
        for possible_subject in doc:
            # print(possible_subject.head)
            # print(possible_subject.dep)
            if possible_subject.dep == nsubj and possible_subject.head.pos == VERB:
                verbs.add(possible_subject.head)
        # print(verbs)
        return verbs


    def simplify_sent(self, sent):
        return self.lemmatize_sent(sent.replace("_", " ").lower())
